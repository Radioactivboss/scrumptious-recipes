from sre_constants import SUCCESS
from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView

from shopping_list.models import ShoppingList

# Create your views here.


class ShoppingListCreateView(CreateView):
    model = ShoppingList
    template_name = "shopping_list/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("shopping_list_detail", pk=plan.id)


class ShoppingListListView(ListView):
    paginate = 3
    model = ShoppingList
    template_name = "shopping_list/list.html"

    def get_queryset(self):
        return ShoppingList.objects.filter(owner=self.request.user)


class ShoppingListDetailView(DetailView):
    model = ShoppingList
    template_name = "shopping_list/detail.html"
    fields = ["name", "date", "recipes"]

    def get_queryset(self):
        return ShoppingList.objects.filter(owner=self.request.user)


class ShoppingListUpdateView(UpdateView):
    model = ShoppingList
    template_name = "shopping_list/edit.html"


class MealPlanDeleteView(DeleteView):
    model = ShoppingList
    template_name = "shopping_list/delete.html"
    success_url = reverse_lazy("shopping_list_list")
