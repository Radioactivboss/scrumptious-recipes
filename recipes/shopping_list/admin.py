from django.contrib import admin

from shopping_list.models import ShoppingList

# Register your models here.


class ShoppingListAdmin(admin.ModelAdmin):
    pass


admin.site.register(ShoppingList, ShoppingListAdmin)
