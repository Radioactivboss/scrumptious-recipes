from django.urls import path
from django.contrib.auth import views as auth_views

from shopping_list.views import (
    ShoppingListCreateView,
    ShoppingListUpdateView,
    ShoppingListDetailView,
    ShoppingListListView,
    ShoppingListDeleteView,
)

urlpatterns = [
    path("", ShoppingListListView.as_view(), name="shopping_list_list"),
    path(
        "<int:pk>/delete/",
        ShoppingListDeleteView.as_view(),
        name="shopping_list_delete",
    ),
    path(
        "<int:pk>/",
        ShoppingListDetailView.as_view(),
        name="shopping_list_detail",
    ),
    path("new/", ShoppingListCreateView.as_view(), name="shopping_list_new"),
    path(
        "<int:pk>/edit/",
        ShoppingListUpdateView.as_view(),
        name="shopping_list_edit",
    ),
]
