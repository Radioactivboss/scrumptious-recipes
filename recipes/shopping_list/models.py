from django.db import models
from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class ShoppingList(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField(auto_now_add=False)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="shopping_list",
        on_delete=models.CASCADE,
        null=True,
    )
    food = models.ForeignKey("FoodItem", on_delete=models.PROTECT)
    recipes = models.ManyToManyField("recipes.Recipe", blank=True)
