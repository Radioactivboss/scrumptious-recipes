from django.urls import path
from django.contrib.auth import views as auth_views

from meal_plans.views import (
    MealPlanCreateView,
    MealPlanUpdateView,
    MealPlanDetailView,
    MealPlanListView,
    MealPlanDeleteView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plans_delete",
    ),
    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plans_detail"),
    path("new/", MealPlanCreateView.as_view(), name="meal_plans_new"),
    path(
        "<int:pk>/edit/", MealPlanUpdateView.as_view(), name="meal_plans_edit"
    ),
]
